# FixEM-Simulator

## Description

This module offers the option of generating fixation eye movements (FixEM) synthetically.

Two different methods are available for this purpose. The resulting trajectories are saved as a .csv-file with the columns "t", "x" and "y". In the case of the mathematical model a boolean column with flags for drift and microsaccades are given as well.

Firstly, a mathematical model can be used, which is based on the random walk algorithm published by Nau et al. \[Nau MA, Ploner SB, Moult EM, Fujimoto JG, Maier AK. Open Source Simulation of Fixational Eye Drift Motion in OCT Scans. [Paper](https://doi.org/10.1007/978-3-658-29267-6_56), [Code](https://github.com/sploner/eye-motion-simulation)\]. That algorithm was extended in this work so that it can also generate microsaccades and tremor.

Secondly, generation is possible using a previously trained GAN, which is based on the code from Severo et al. \[Severo D, Amaro F, Hruschka Jr ER, Costa ASDM. Ward2ICU: A Vital Signs Dataset of Inpatients from the General Ward. [Paper](https://doi.org/10.48550/arXiv.1910.00752), [Code](https://github.com/3778/Ward2ICU)\].

The modules are organised as follows:

- The "GeneratingTraces_MathematicalModel" folder provides all the modules required to generate FixEM based on the mathematical model. It contains the "RandomWalk.py" file, which provides the interface for operation with the `RandomWalk()` function.

- The "GeneratingTraces_RCGAN" folder provides all the functions for training and generating new trajectories using RCGAN. This contains functions for training, `run_experiment()` and for generating new trajectories using the file "generate_dataset.py".

Other files included are the label files from the freely accessible datasets of the Roorda Labobarory and the GazeBase
dataset.

The "ProcessingData" folder also contains various functions for processing and visualising the data.

All functions can be called via a GUI. This offers the option of easily generating trajectories. It should be noted that the use of the GUI is a simplified version of the generation option; not all parameters can be configured there. The respective function of the module must be used for more precise configuration of the trajectories. The configuration options offered provide the most necessary parameters.

## Installation

In addition to the [_Eye motion simulation_](https://github.com/sploner/eye-motion-simulation) (by Stefan Ploner and Merlin Nau), this module also makes use of the [_Microsaccade Toolbox for Python_](https://github.com/lschwetlick/EngbertMicrosaccadeToolbox) (Python implementation by Lisa Schwetlick and Paul Schmitthäuser, original R implementation by Ralf Engbert, Petra Sinn, Konstantin Mergenthaler and Hans Trukenbrod). Both repositiories are included as Git submodules.

To get the submodules, either get them immediately with the initial clone:

`git clone --recurse-submodules https://gitlab.kit.edu/kit/iai/ml4time/cornea/FixEM-Simulator`

or separately afterwards:

`git submodule update --init --recursive`

Finally, install the dependencies from `requirements.txt` and you're ready to start.

## Authors

- Fabian Anzlinger
- [Stephan Allgeier](https://www.iai.kit.edu/english/2154_2149.php)
